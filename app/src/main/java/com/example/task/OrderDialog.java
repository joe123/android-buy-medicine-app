package com.example.task;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

public class OrderDialog extends AppCompatDialogFragment {

    private EditText editTextQuantity;
    private EditText editTextPrice;
    private Spinner spinner;
    private String name;
    private OrderDialogListener listener;


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.order_dialog, null);

        editTextQuantity = view.findViewById(R.id.quantity_text);
        editTextPrice = view.findViewById(R.id.price_text);
        spinner = view.findViewById(R.id.spinner);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1) {
                    name = "Panadol";
                } else if (position == 2) {
                    name = "Night and Day";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        builder.setView(view)
                .setTitle("Pick medicine")
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                })
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (editTextPrice.getText().toString().matches("") || editTextQuantity.getText().toString().matches("")) {
                            Toast.makeText(getContext(), "Please Complete Data", Toast.LENGTH_SHORT).show();
                        } else {
                            String quantity = editTextQuantity.getText().toString();
                            String price = editTextPrice.getText().toString();
                            listener.applyTexts(name, quantity, price);
                        }
                    }
                });


        ArrayAdapter<String> myAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.medicines));
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(myAdapter);

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (OrderDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement OrderDialogListener");
        }
    }

    public interface OrderDialogListener {
        void applyTexts(String medicineName, String quantity, String price);
    }
}
