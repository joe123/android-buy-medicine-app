package com.example.task;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Map;

public class AddActivity extends AppCompatActivity implements OrderDialog.OrderDialogListener {

    private FloatingActionButton floatingActionButton;
    private TextView totalPriceText;

    private ListView listView;
    private ArrayList<String> medicineDataList = new ArrayList<>();
    private Button doneBtn;
    private int listSize = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        floatingActionButton = findViewById(R.id.floating_btn);
        totalPriceText = findViewById(R.id.total_price);
        doneBtn = findViewById(R.id.done_btn);

        listView = findViewById(R.id.list_item);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderDialog exampleDialog = new OrderDialog();
                exampleDialog.show(getSupportFragmentManager(), "example dialog");
            }
        });

        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AddActivity.this, MainActivity.class);
                i.putExtra("orders_number", listSize);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    public void applyTexts(String medicineName, String quantity, String price) {
        int total = Integer.parseInt(totalPriceText.getText().toString());
        total += Integer.parseInt(price);
        totalPriceText.setText(String.valueOf(total));

        String data = medicineName + "  quantity : " + quantity;
        medicineDataList.add(data);
        listSize++;

        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, medicineDataList);

        listView.setAdapter(arrayAdapter);

    }
}